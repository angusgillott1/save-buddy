using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using SaveBuddy;
using System.Text.RegularExpressions;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddDbContext<SavedItemDb>(opt => opt.UseInMemoryDatabase("SavedItemList"));
builder.Services.AddDatabaseDeveloperPageExceptionFilter();
var app = builder.Build();

app.MapPost("/save", async (KeyAndUnencryptedValue keyAndValue, SavedItemDb db) =>
{
    string keyValidity = KeyValidator.checkKey(keyAndValue.Key);
    if(keyValidity != "Key valid.")
    {
        return keyValidity;
    }

    try
    {
        var newSavedItem = new SavedItem();

        newSavedItem.HalfKey = KeyValidator.getHalfKey(keyAndValue.Key);
        newSavedItem.EncryptedValue = EncryptDecrypt.encryptString(keyAndValue.Key, keyAndValue.Value);

        var existingMatches = await db.SavedItems.Where(item => item.HalfKey == KeyValidator.getHalfKey(keyAndValue.Key)).ToListAsync();

        if(existingMatches.Count != 0)
        {
            existingMatches.First().EncryptedValue = newSavedItem.EncryptedValue;
        }
        else
        {
            db.SavedItems.Add(newSavedItem);
        }

        await db.SaveChangesAsync();
    }
    catch
    {
        return "Save failed due to error.";
    }

    return "Value saved.";
});

app.MapGet("/retrieve/{key}", async (string key, SavedItemDb db) =>
{
    string keyValidity = KeyValidator.checkKey(key);
    if(keyValidity != "Key valid.")
    {
        return keyValidity;
    }

    var matchingItems = await db.SavedItems.Where(item => item.HalfKey == KeyValidator.getHalfKey(key)).ToListAsync();
    if (matchingItems.Count == 0)
        return "Not found.";

    try
    {
        return EncryptDecrypt.decryptString(key, matchingItems.First().EncryptedValue);
    }
    catch
    {
        return "Retrieve failed due to error.";
    }
});

app.MapGet("/generate-key", () =>
{
    int KEY_LENGTH = 16;

    char[] validCharacters =
    {
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 't', 'v', 'w', 'x', 'y', 'z',
        'A', 'B', 'D', 'E', 'F', 'G', 'H', 'J', 'M', 'N', 'P', 'Q', 'R', 'T', 'Y'
    };

    string generatedString = "";
    Random rng = new Random();

    for(int i = 0; i < KEY_LENGTH; i++)
    {
        generatedString += validCharacters[rng.Next(validCharacters.Length)];
    }

    return generatedString;
});

app.Run();
