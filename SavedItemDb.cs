﻿using Microsoft.EntityFrameworkCore;
using SaveBuddy;

class SavedItemDb : DbContext
{
    public SavedItemDb(DbContextOptions<SavedItemDb> options)
        : base(options) { }

    public DbSet<SavedItem> SavedItems => Set<SavedItem>();
}
