﻿using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace SaveBuddy
{
    public class SavedItem
    {
        public int Id { get; set; }
        public string HalfKey { get; set; }
        public string EncryptedValue { get; set; }
    }

    public class KeyAndUnencryptedValue
    { 
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class KeyValidator
    {
        public static string checkKey(string key)
        {
            if (key.Length != 16)
                return "Key must be exactly 16 characters.";

            if (!Regex.IsMatch(key, @"^[a-zA-Z]+$"))
                return "Key must only contain letters.";

            return "Key valid.";
        }

        public static string getHalfKey(string key)
        {
            return key.Substring(0, key.Length / 2);
        }
    }

    public class EncryptDecrypt
    {
        public static string encryptString(string key, string plainText)
        {
            byte[] array;

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = new byte[16];

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(plainText);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }

            return Convert.ToBase64String(array);
        }

        public static string decryptString(string key, string cipherText)
        {
            byte[] buffer = Convert.FromBase64String(cipherText);

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = new byte[16];
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }
    }
}
